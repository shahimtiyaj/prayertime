package com.bytelab.prayertime;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.bytelab.prayertime.Fragments.FragmentTasbeeh;
import com.bytelab.prayertime.Fragments.InitialConfigFragment;
import com.bytelab.prayertime.Fragments.LocationHelper;
import com.bytelab.prayertime.Fragments.SalaatTimesFragment;
import com.bytelab.prayertime.Utils.AppSettings;
import com.bytelab.prayertime.Utils.ScreenUtils;
import com.bytelab.prayertime.Widget.FragmentStatePagerAdapter;
import com.bytelab.prayertime.Widget.SlidingTabLayout;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;


public class SalaatTimesActivity extends AppCompatActivity implements Constants,
        InitialConfigFragment.OnOptionSelectedListener, ViewPager.OnPageChangeListener,
        LocationHelper.LocationCallback {
    InterstitialAd mInterstitialAd;
    private LocationHelper mLocationHelper;
    private Location mLastLocation = null;
    private ViewPager mPager;
    private ScreenSlidePagerAdapter mAdapter;
    private SlidingTabLayout mTabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        AppSettings settings = AppSettings.getInstance(this);
        //INIT APP
        if (!settings.getBoolean(AppSettings.Key.IS_INIT)) {
            settings.set(settings.getKeyFor(AppSettings.Key.IS_ALARM_SET, 0), true);
            settings.set(settings.getKeyFor(AppSettings.Key.IS_FAJR_ALARM_SET, 0), true);
            settings.set(settings.getKeyFor(AppSettings.Key.IS_DHUHR_ALARM_SET, 0), true);
            settings.set(settings.getKeyFor(AppSettings.Key.IS_ASR_ALARM_SET, 0), true);
            settings.set(settings.getKeyFor(AppSettings.Key.IS_MAGHRIB_ALARM_SET, 0), true);
            settings.set(settings.getKeyFor(AppSettings.Key.IS_ISHA_ALARM_SET, 0), true);
            settings.set(AppSettings.Key.USE_ADHAN, true);
            settings.set(AppSettings.Key.IS_INIT, true);
        }

        setContentView(R.layout.activity_salaat_times);
        ScreenUtils.lockOrientation(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(Color.parseColor("#4CAF50"));
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        //Full screen add init---------------------------------------------
        mInterstitialAd = new InterstitialAd(getApplicationContext());
        mInterstitialAd.setAdUnitId(getString(R.string.fullscreenad));
        requestNewInterstitial();


        mLocationHelper = (LocationHelper) getFragmentManager().findFragmentByTag(LOCATION_FRAGMENT);

        // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
        mAdapter = new ScreenSlidePagerAdapter(getFragmentManager(), 0);

        // Assigning ViewPager View and setting the adapter
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);
        mPager.addOnPageChangeListener(this);

        // Assiging the Sliding Tab Layout View
        mTabs = (SlidingTabLayout) findViewById(R.id.tabs);
        mTabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Color for the Scroll bar indicator of the Tab View
/*
    mTabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
      @Override
      public int getIndicatorColor(int position) {
        return getResources().getColor(R.color.teal_accent);
      }
    });
*/
        mTabs.setSelectedIndicatorColors(getResources().getColor(android.R.color.primary_text_dark));
        mTabs.setTextColor(android.R.color.primary_text_dark);

        // Setting the ViewPager For the SlidingTabsLayout
        mTabs.setViewPager(mPager);

        if (mLocationHelper == null) {
            mLocationHelper = LocationHelper.newInstance();
            getFragmentManager().beginTransaction().add(mLocationHelper, LOCATION_FRAGMENT).commit();
        }

//    if (!settings.getBoolean(AppSettings.Key.IS_TNC_ACCEPTED, false)) {
//      getWindow().getDecorView().postDelayed(new Runnable() {
//        @Override
//        public void run() {
//          Intent intent = new Intent(SalaatTimesActivity.this, TermsAndConditionsActivity.class);
//          overridePendingTransition(R.anim.enter_from_bottom, R.anim.no_animation);
//          startActivityForResult(intent, REQUEST_TNC);
//        }
//      }, 2000);
//    }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mLastLocation == null) {
            fetchLocation();
        }

    }

    @Override
    protected void onDestroy() {
        //Just to be sure memory is cleaned up.
        mPager.removeOnPageChangeListener(this);
        mPager = null;
        mAdapter = null;
        mTabs = null;
        mLastLocation = null;

        super.onDestroy();
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .build();

        mInterstitialAd.loadAd(adRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_salaat_times, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startOnboardingFor(0);

            //Load full screen Add-----------------------
            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            }

            return true;
//    } else if (id == R.id.action_terms) {
//      Intent intent = new Intent(SalaatTimesActivity.this, TermsAndConditionsActivity.class);
//      intent.putExtra(TermsAndConditionsActivity.EXTRA_DISPLAY_ONLY, true);
//      overridePendingTransition(R.anim.enter_from_bottom, R.anim.no_animation);
//      startActivityForResult(intent, REQUEST_TNC);
        }

        if (id == R.id.rate) {
            //Load full screen Add-----------------------
            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            }

            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://details?id=com.bytelab.prayertime"));
            startActivity(intent);
        }

        if (id == R.id.share) {
            //Load full screen Add-----------------------
            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            }
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, " Salaat Times");
            String sAux = "\nSalaat Times\n\n";
            sAux = sAux + "https://play.google.com/store/apps/details?id=com.bytelab.prayertime\n\n";
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            startActivity(Intent.createChooser(i, "Select One"));
        }

        if (id == R.id.action_feedback) {
            //Load full screen Add-----------------------
            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            }
            Intent email = new Intent(Intent.ACTION_SEND);
            email.setType("text/email");
            email.putExtra(Intent.EXTRA_EMAIL, new String[]{"shahimtiyaj94@gmail.com"});
            email.putExtra(Intent.EXTRA_SUBJECT, "Feedback about Salat Times");
            email.putExtra(Intent.EXTRA_TEXT, "Hello, " + "\n");
            startActivity(Intent.createChooser(email, "Send Feedback:"));
        }

        if (id == R.id.action_wifi) {
            //Load full screen Add-----------------------
            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            }
            if (!isNetworkAvailable(getApplicationContext())) {
                POExistsALert();
            } else {
                Toast.makeText(getApplicationContext(), "Network is available!", Toast.LENGTH_LONG).show();
            }

        }
        return super.onOptionsItemSelected(item);
    }

    private void startOnboardingFor(int index) {
        Intent intent = new Intent(getApplicationContext(), OnboardingActivity.class);
        intent.putExtra(OnboardingActivity.EXTRA_CARD_INDEX, index);
        startActivityForResult(intent, REQUEST_ONBOARDING);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    // All required changes were successfully made
                    fetchLocation();
                    break;
                case Activity.RESULT_CANCELED:
                    // The user was asked to change settings, but chose not to
                    onLocationSettingsFailed();
                    break;
                default:
                    onLocationSettingsFailed();
                    break;
            }
        } else if (requestCode == REQUEST_ONBOARDING) {
            if (resultCode == RESULT_OK) {
                onUseDefaultSelected();
            }
        } else if (requestCode == REQUEST_TNC) {
            if (resultCode == RESULT_CANCELED) {
                finish();
            } else {
                AppSettings settings = AppSettings.getInstance(this);
                settings.set(AppSettings.Key.IS_TNC_ACCEPTED, true);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    private void fetchLocation() {
        if (mLocationHelper != null) {
            mLocationHelper.checkLocationPermissions();
        }
    }

    @Override
    public void onLocationSettingsFailed() {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        // NOT THE BEST SOLUTION, THINK OF SOMETHING ELSE
        mAdapter = new ScreenSlidePagerAdapter(getFragmentManager(), 0);
        mPager.setAdapter(mAdapter);
    }

    @Override
    public void onConfigNowSelected(int num) {
        startOnboardingFor(num);
    }

    @Override
    public void onUseDefaultSelected() {
        if (mLastLocation != null) {
            // NOT THE BEST SOLUTION, THINK OF SOMETHING ELSE
            mAdapter = new ScreenSlidePagerAdapter(getFragmentManager(), 0);
            mPager.setAdapter(mAdapter);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        private int mCardIndex;
        //  public KaabaLocatorFragment mKaabaLocatorFragment;

        public ScreenSlidePagerAdapter(FragmentManager fm, int index) {
            super(fm);
            mCardIndex = index;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    if (AppSettings.getInstance(getApplicationContext()).isDefaultSet()) {
                        return SalaatTimesFragment.newInstance(mCardIndex, mLastLocation);
                    } else {
                        return InitialConfigFragment.newInstance();
                    }
                case 1:
                    return FragmentTasbeeh.newInstance();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0) {
                return getString(R.string.salaat_times);
            } else {
                return getString(R.string.tasbeeh);
            }
        }


    }

    public void EnableWiFi() {

        WifiManager wifimanager = (WifiManager) getApplicationContext().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        wifimanager.setWifiEnabled(true);

    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void POExistsALert() {
        new AlertDialog.Builder(this)
                .setIcon(R.drawable.ic_network_wifi)
                .setTitle("Attention !")
                .setMessage("No Internet connection.Make sure that Wi-Fi or " +
                        "cellular mobile data is turned on for best results.")
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        EnableWiFi();
                    }

                }).show();
    }
}
