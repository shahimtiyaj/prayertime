package com.bytelab.prayertime.Fragments;


import android.app.Fragment;
import android.view.View;

/**
 * A simple {@link Fragment} subclass.
 */
public abstract class OnboardingBaseFragment extends Fragment implements View.OnClickListener {

  public OnboardingBaseFragment() {
    // Required empty public constructor
  }

}
