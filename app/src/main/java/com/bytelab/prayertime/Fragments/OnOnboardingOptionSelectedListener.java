package com.bytelab.prayertime.Fragments;

public interface OnOnboardingOptionSelectedListener {
    public void onOptionSelected();
}